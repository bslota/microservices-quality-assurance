package com.bartslota.availability.testcontainers


import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.TopicBuilder

/**
 * @author bslota on 06/11/2022
 */
@Configuration
class KafkaTopicConfiguration {

    @Bean
    def assetAvailabilityTopic() {
        return TopicBuilder.name("asset-availability").partitions(1).build()
    }

}
