package com.bartslota.availability.infrastructure.events


import com.bartslota.availability.domain.AssetId
import com.bartslota.availability.domain.OwnerId
import com.bartslota.availability.events.AssetActivated
import com.bartslota.availability.events.AssetLocked
import com.bartslota.availability.events.AssetRegistered
import com.bartslota.availability.events.AssetUnlocked
import com.bartslota.availability.events.AssetWithdrawn
import com.bartslota.availability.infrastructure.kafka.KafkaEventsTestListener
import org.awaitility.Awaitility
import org.springframework.beans.factory.annotation.Autowired

import static com.bartslota.availability.PredefinedPollingConditions.SHORT_WAIT
import static java.util.concurrent.TimeUnit.SECONDS

/**
 * @author bslota on 09/01/2022
 */
trait AssetAvailabilityEventsKafkaSupport {

    @Autowired
    KafkaEventsTestListener kafkaEventsTestListener

    void assetRegisteredEventWasPublishedFor(AssetId assetId) {
        SHORT_WAIT.eventually {
            assert kafkaEventsTestListener.containsEventFulfilling {
                event -> (event instanceof AssetRegistered) && AssetId.of(event.assetId) == assetId
            }
        }
    }

    void assetWithdrawnEventWasPublishedFor(AssetId assetId) {
        SHORT_WAIT.eventually {
            kafkaEventsTestListener.containsEventFulfilling {
                event -> (event instanceof AssetWithdrawn) && AssetId.of(event.assetId) == assetId
            }
        }
    }

    void assetActivatedEventWasPublishedFor(AssetId assetId) {
        SHORT_WAIT.eventually {
            assert kafkaEventsTestListener.containsEventFulfilling {
                event -> (event instanceof AssetActivated) && AssetId.of(event.assetId) == assetId
            }
        }
    }

    void assetLockedEventWasPublishedFor(AssetId assetId, OwnerId ownerId) {
        Awaitility
                .await()
                .atMost(3, SECONDS)
                .until(() ->
                        kafkaEventsTestListener.containsEventFulfilling {
                            it ->
                                (it instanceof AssetLocked)
                                        && AssetId.of(it.assetId) == assetId
                                        && OwnerId.of(it.ownerId) == ownerId
                        }
                )
    }

    void assetUnlockedEventWasPublishedFor(AssetId assetId, OwnerId ownerId) {
        Awaitility
                .await()
                .atMost(3, SECONDS)
                .until(() ->
                        kafkaEventsTestListener.containsEventFulfilling {
                            it ->
                                (it instanceof AssetUnlocked)
                                        && AssetId.of(it.assetId) == assetId
                                        && OwnerId.of(it.ownerId) == ownerId
                        }
                )
    }
}
