package com.bartslota.availability.infrastructure.events

import com.bartslota.availability.IntegrationSpec
import com.bartslota.availability.application.AssetAvailabilityStoreSupport
import com.bartslota.availability.application.AvailabilityService
import com.bartslota.availability.auth.AuthSupport
import com.bartslota.availability.domain.AssetAvailability
import com.bartslota.availability.domain.AssetAvailabilityRepository
import com.bartslota.availability.domain.AssetId
import com.bartslota.availability.domain.OwnerId
import com.bartslota.availability.infrastructure.kafka.KafkaEventsTestListener
import org.springframework.beans.factory.annotation.Autowired

import static com.bartslota.availability.domain.AssetIdFixture.someAssetId
import static com.bartslota.availability.domain.DurationFixture.someValidDuration
import static com.bartslota.availability.domain.OwnerIdFixture.someOwnerId
import static java.time.LocalDateTime.now

/**
 * @author bslota on 30/11/2021
 */
class AssetAvailabilityEventsIT extends IntegrationSpec implements AssetAvailabilityStoreSupport,
        AuthSupport, AssetAvailabilityEventsKafkaSupport {

    @Autowired
    private AssetAvailabilityRepository assetAvailabilityRepository

    @Autowired
    private AvailabilityService availabilityService

    def setup() {
        kafkaEventsTestListener.cleanup()
    }

    def "should emit AssetRegistered event when successfully registered asset"() {
        given:
            AssetId assetId = someAssetId()

        and:
            authorizedAsAdmin()

        when:
            availabilityService.registerAssetWith(assetId)

        then:
            assetRegisteredEventWasPublishedFor(assetId)
    }

    def "should emit AssetWithdrawn event when asset was successfully withdrawn"() {
        given:
            AssetAvailability asset = existingAsset()

        and:
            authorizedAsAdmin()

        when:
            availabilityService.withdraw(asset.id())

        then:
            assetWithdrawnEventWasPublishedFor(asset.id())
    }

    def "should emit AssetActivated event when asset was successfully activated"() {
        given:
            AssetAvailability asset = existingAsset()

        and:
            authorizedAsAdmin()

        when:
            availabilityService.activate(asset.id())

        then:
            assetActivatedEventWasPublishedFor(asset.id())
    }

    def "should emit AssetLocked event when asset was successfully locked"() {
        given:
            AssetAvailability asset = activatedAsset()
            OwnerId ownerId = someOwnerId()

        and:
            authorizedAsCustomer()

        when:
            availabilityService.lock(asset.id(), ownerId, someValidDuration())

        then:
            assetLockedEventWasPublishedFor(asset.id(), ownerId)
    }

    def "should emit AssetLocked event when asset was successfully locked indefinitely"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetAvailability asset = assetLockedBy(ownerId)

        and:
            authorizedAsCustomer()

        when:
            availabilityService.lockIndefinitely(asset.id(), ownerId)

        then:
            assetLockedEventWasPublishedFor(asset.id(), ownerId)
    }

    def "should emit AssetUnlocked event when asset was successfully unlocked"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            authorizedAsCustomer()

        and:
            AssetAvailability asset = assetLockedBy(ownerId)

        when:
            availabilityService.unlock(asset.id(), ownerId, now())

        then:
            assetUnlockedEventWasPublishedFor(asset.id(), ownerId)
    }

    @Override
    AssetAvailabilityRepository repository() {
        return assetAvailabilityRepository
    }
}
