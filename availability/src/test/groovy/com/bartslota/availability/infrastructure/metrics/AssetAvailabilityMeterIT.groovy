package com.bartslota.availability.infrastructure.metrics

import com.bartslota.availability.IntegrationSpec
import com.bartslota.availability.application.AssetAvailabilityStoreSupport
import com.bartslota.availability.application.AvailabilityService
import com.bartslota.availability.auth.AuthSupport
import com.bartslota.availability.domain.AssetAvailability
import com.bartslota.availability.domain.AssetAvailabilityRepository
import com.bartslota.availability.domain.OwnerId
import io.micrometer.core.instrument.Metrics
import io.micrometer.core.instrument.Tag
import org.springframework.beans.factory.annotation.Autowired

import static com.bartslota.availability.domain.DurationFixture.someValidDuration
import static com.bartslota.availability.domain.OwnerIdFixture.someOwnerId

/**
 * @author bslota on 11/01/2022
 */
class AssetAvailabilityMeterIT extends IntegrationSpec implements AssetAvailabilityStoreSupport,
        AuthSupport {

    @Autowired
    private AssetAvailabilityRepository assetAvailabilityRepository

    @Autowired
    private AvailabilityService availabilityService

    def setup() {
        Metrics.globalRegistry.clear()
    }

    def "should count the asset lock when lock is applied successfully"() {
        given:
            AssetAvailability asset = activatedAsset()
            OwnerId ownerId = someOwnerId()

        and:
            authorizedAsCustomer()

        when:
            availabilityService.lock(asset.id(), ownerId, someValidDuration())

        then:
            Metrics.globalRegistry
                    .counter("asset.locks",
                            [Tag.of("assetId", asset.id().asString()),
                             Tag.of("ownerId", ownerId.asString())])
                    .count() == 1
    }

    def "should not count the asset lock when lock application has failed"() {
        given:
            AssetAvailability asset = existingAsset()
            OwnerId ownerId = someOwnerId()

        and:
            authorizedAsCustomer()

        when:
            availabilityService.lock(asset.id(), ownerId, someValidDuration())

        then:
            Metrics.globalRegistry
                    .counter("asset.locks",
                            [Tag.of("assetId", asset.id().asString()),
                             Tag.of("ownerId", ownerId.asString())])
                    .count() == 0
    }

    @Override
    AssetAvailabilityRepository repository() {
        return assetAvailabilityRepository
    }
}
