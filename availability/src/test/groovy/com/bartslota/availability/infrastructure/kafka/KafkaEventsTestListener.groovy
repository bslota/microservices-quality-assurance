package com.bartslota.availability.infrastructure.kafka

import com.bartslota.availability.events.DomainEvent
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.config.KafkaListenerEndpointRegistry
import org.springframework.kafka.test.utils.ContainerTestUtils
import org.springframework.stereotype.Component

import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

/**
 * @author bslota on 09/01/2022
 */
@Component
@Slf4j
class KafkaEventsTestListener {

    @Autowired
    private ObjectMapper objectMapper

    @Autowired
    private KafkaListenerEndpointRegistry registry

    private BlockingQueue<DomainEvent> capturedEvents = new LinkedBlockingQueue<>()

    void cleanup() {
        ContainerTestUtils.waitForAssignment(registry.getListenerContainer("assetAvailability"), 1)
        capturedEvents.clear()
    }

    @KafkaListener(id = "assetAvailability", idIsGroup = false, topics = ["asset-availability"])
    void handle(String event) {
        capturedEvents.offer(objectMapper.readValue(event, DomainEvent.class))
    }

    boolean containsEventFulfilling(Closure<Boolean> findingClosure) {
        capturedEvents.any {findingClosure(it)}
    }
}
