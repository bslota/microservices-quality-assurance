package com.bartslota.availability.domain


import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric

/**
 * @author bslota on 22/11/2021
 */
class OwnerIdFixture {

    static OwnerId someOwnerId() {
        OwnerId.of(randomAlphanumeric(10))
    }
}
