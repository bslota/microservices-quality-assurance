package com.bartslota.payments;

/**
 * @author bslota on 09/01/2022
 */
public record Account(AccountId id, Money balance) {

}
