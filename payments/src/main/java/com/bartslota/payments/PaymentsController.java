package com.bartslota.payments;

import java.net.URI;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * @author bslota on 09/01/2022
 */
@RestController
@RequestMapping("/api/accounts/{accountId}")
class PaymentsController {

    @PostMapping("/locks")
    ResponseEntity<?> handle(@PathVariable AccountId accountId, @RequestBody LockRequest request) {
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(UUID.randomUUID())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/locks/{lockId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void handle(@PathVariable AccountId accountId) {
        //
    }

    @PostMapping("/chargings")
    ResponseEntity<?> handle(@PathVariable AccountId accountId, @RequestBody ChargingRequest request) {
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(UUID.randomUUID())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping
    ResponseEntity<Account> get(@PathVariable AccountId accountId) {
        return ResponseEntity.ok().build();
    }
}
