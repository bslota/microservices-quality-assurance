package com.bartslota.payments;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author bslota on 09/01/2022
 */
public record AccountId(@JsonValue String value) {

}
