package com.bartslota.c4.styles;

import java.util.function.Function;

import com.structurizr.view.ElementStyle;
import com.structurizr.view.Styles;

public interface StyleProvider extends Function<Styles, ElementStyle> {

}
