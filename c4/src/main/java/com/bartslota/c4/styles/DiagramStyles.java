package com.bartslota.c4.styles;

import java.util.List;

import com.structurizr.Workspace;
import com.structurizr.view.Styles;

public record DiagramStyles(Workspace workspace) {

    public void applyStyles(List<StyleProvider> styleProvider) {
        styleProvider.forEach(this::applyStyle);
    }

    public void applyStyle(StyleProvider styleProvider) {
        final Styles styles = workspace.getViews().getConfiguration().getStyles();
        styleProvider.apply(styles);
    }
}
