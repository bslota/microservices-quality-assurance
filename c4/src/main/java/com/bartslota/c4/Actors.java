package com.bartslota.c4;

import com.structurizr.Workspace;
import com.structurizr.model.Model;
import com.structurizr.model.Person;

class Actors {

    final Person customer;
    final Person employee;

    private Actors(Model model) {
        customer = model.addPerson("User", "A user of E-bike services");
        employee = model.addPerson("Employee", "E-bike employee");
    }

    static Actors createFor(Workspace workspace) {
        return new Actors(workspace.getModel());
    }

}
