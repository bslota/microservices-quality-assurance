# README
## How to generate C4 diagrams
Diagrams are created with [Structurizr for Java](https://github.com/structurizr/java). In order to visualize them, we have two options:
- Remote - by exporting the diagram to [Structurizr](structurizr.com)
- Lite - by exporting the diagram to your local [Structurizr Lite instance](https://structurizr.com/help/lite)

### Structurizr
If you want to use web version of structurizr, you need to create an account and get API access. When you have it, you need to create `secret.properties` file under `src/main/resources/` directory, and fill it with following information:

```properties
workspace.id=
workspace.url=https://api.structurizr.com
api.key=
api.secret=
```

Then you can execute:

```shell
mvn exec:java -Dexec.args="remote"
```

Diagrams will be exported to your account.

### Structurizr Lite
Structurizr Lite is a free app that can be used to view/edit diagrams locally. In order to run it, you just need to download the image:

```shell
docker pull structurizr/lite
```

You can run it with:

```shell
docker run -it --rm -p 8080:8080 -v PATH:/usr/local/structurizr structurizr/lite
```

where `PATH` placeholder should be replaced with the path to local directory of your choice, like `/Users/myuser/structurizr`.

Before you run it, you need to place `workspace.json` file in mentioned directory. The file should contain your diagram definition written in DSL. You can write that definition in any text editor, or export it from client app. In our case, we can just run the following command:

```shell
cd c4 #move to c4 directory
mvn exec:java -Dexec.args="lite"
```

