package com.bartslota.reservations


import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder

import static java.nio.charset.Charset.defaultCharset
import static org.springframework.http.HttpHeaders.AUTHORIZATION
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

/**
 * @author bslota on 07/01/2022
 */
class MakeReservationCommandSendingBuilder {
    private MockMvc mockMvc
    String vehicleId
    String username = "ADAM_123"
    String password = "pass_123"

    MakeReservationCommandSendingBuilder(MockMvc mockMvc, String vehicleId) {
        this.mockMvc = mockMvc
        this.vehicleId = vehicleId
    }

    MakeReservationCommandSendingBuilder withoutCredentials() {
        this.username = null
        this.password = null
        this
    }

    MakeReservationCommandSendingBuilder withCredentialsOf(String username, String password) {
        this.username = username
        this.password = password
        this
    }

    ResultActions send() {
        MockHttpServletRequestBuilder request = post("/api/reservations")
        if (username && password) {
            request.header(AUTHORIZATION, httpBasicHeaderFor(username, password))
        }
        request.contentType(APPLICATION_JSON)
                .content("""
                                {
                                    "vehicleId" : "${vehicleId}"
                                }
                            """)
        mockMvc.perform(request)
    }

    private static String httpBasicHeaderFor(String username, String pass) {
        "Basic " + Base64.getEncoder().encodeToString("$username:$pass".getBytes(defaultCharset()))
    }
}
