package com.bartslota.reservations


import groovy.transform.SelfType

/**
 * @author bslota on 06/01/2022
 */
@SelfType(IntegrationSpec)
trait ReservationsWebSupport {

    MakeReservationCommandSendingBuilder makeReservationFor(String vehicleId) {
        return new MakeReservationCommandSendingBuilder(mockMvc, vehicleId)
    }
}