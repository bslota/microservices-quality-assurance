package com.bartslota.reservations


import org.springframework.test.web.servlet.ResultActions

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson
import static com.github.tomakehurst.wiremock.client.WireMock.post
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import static org.springframework.http.HttpHeaders.CONTENT_TYPE
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * @author bslota on 30/11/2021
 */
class ReservationsIT extends IntegrationSpec implements ReservationsWebSupport {

    def "should lock asset when creating reservation"() {
        when:
            ResultActions result = makeReservationFor("correct-asset-id").send()

        then:
            result.andExpect(status().isAccepted())
    }

    def "should fail to make reservation when asset lock is rejected with 422"() {
        when:
            ResultActions result = makeReservationFor("already-locked-asset-id").send()

        then:
            result.andExpect(status().isUnprocessableEntity())
    }

    def "should fail to make reservation when user is not authorized in availability service"() {
        when:
            ResultActions result = makeReservationFor("correct-asset-id")
                    .withoutCredentials()
                    .send()

        then:
            result.andExpect(status().isUnauthorized())
    }

    def "should fail to make reservation when user is forbidden in availability service"() {
        when:
            ResultActions result = makeReservationFor("correct-asset-id")
                    .withCredentialsOf("forbidden_user", "pass_123")
                    .send()

        then:
            result.andExpect(status().isForbidden())
    }

    def "should fail to make reservation when connection with availability service is timed out"() {
        given:
            stubFor(post(urlPathEqualTo("/api/assets/commands"))
                    .withRequestBody(equalToJson("""{
                                                              "assetId": "connection-timeout-asset-id-for-programmatic-stub",
                                                              "durationInMinutes": 10,
                                                              "type" : "LOCK"
                                                            }"""))
                    .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                    .withBasicAuth("ADAM_123", "pass_123")
                    .willReturn(aResponse()
                            .withStatus(202)
                            .withFixedDelay(1000)))

        when:
            ResultActions result = makeReservationFor("connection-timeout-asset-id-for-programmatic-stub")
                    .withCredentialsOf("ADAM_123", "pass_123")
                    .send()

        then:
            result.andExpect(status().isRequestTimeout())
    }

    def "should fail to make reservation when connection with availability service is closed"() {
        when:
            ResultActions result = makeReservationFor("connection-closed-asset-id")
                    .withCredentialsOf("ADAM_123", "pass_123")
                    .send()

        then:
            result.andExpect(status().isServiceUnavailable())
    }

    def "should fail to make reservation when connection with availability service returns internal server error"() {
        when:
            ResultActions result = makeReservationFor("asset-id-causing-internal-server-error")
                    .withCredentialsOf("ADAM_123", "pass_123")
                    .send()

        then:
            result.andExpect(status().isServiceUnavailable())
    }

    def "should fail to make reservation when connection with availability service is unavailable"() {
        when:
            ResultActions result = makeReservationFor("asset-id-when-service-is-unavailable")
                    .withCredentialsOf("ADAM_123", "pass_123")
                    .send()

        then:
            result.andExpect(status().isServiceUnavailable())
    }
}
