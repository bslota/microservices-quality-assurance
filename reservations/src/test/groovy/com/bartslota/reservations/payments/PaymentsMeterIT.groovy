package com.bartslota.reservations.payments

import com.bartslota.reservations.IntegrationSpec
import io.micrometer.core.instrument.Metrics
import io.micrometer.core.instrument.Tag
import org.springframework.beans.factory.annotation.Autowired

import static com.bartslota.reservations.RandomUtils.random

/**
 * @author bslota on 11/01/2022
 */
class PaymentsMeterIT extends IntegrationSpec {

    @Autowired
    private PaymentsFacade paymentsFacade

    def setup() {
        Metrics.globalRegistry.clear()
    }

    def "should collect time metric when payment facade is called"() {
        given:
            String accountIdValue = random("accountId")

        and:
            LockRequest lockRequest = someLockRequest()

        when:
            paymentsFacade.send(new AccountId(accountIdValue), lockRequest)

        then:
            Metrics.globalRegistry
                    .timer("account.locks",
                            [Tag.of("accountId", accountIdValue)])
                    .count() == 1
    }

    private static LockRequest someLockRequest() {
        new LockRequest(new Money(BigDecimal.valueOf(120), "EUR"), "reservation")
    }


}
