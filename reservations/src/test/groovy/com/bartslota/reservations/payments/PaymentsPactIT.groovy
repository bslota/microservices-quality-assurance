package com.bartslota.reservations.payments

import au.com.dius.pact.consumer.PactVerificationResult
import au.com.dius.pact.consumer.groovy.PactBuilder
import au.com.dius.pact.core.model.PactSpecVersion
import com.bartslota.reservations.IntegrationSpec

import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

import static au.com.dius.pact.core.model.PactSpecVersion.V3
import static java.net.http.HttpRequest.BodyPublishers.ofString
import static java.net.http.HttpResponse.BodyHandlers.discarding

/**
 * @author bslota on 07/11/2022
 */
class PaymentsPactIT extends IntegrationSpec {

    def "should successfully lock resources on the account"() {
        given:
            PactBuilder pactBuilder = new PactBuilder()
            pactBuilder {
                serviceConsumer "Reservation Service"
                hasPactWith "Payments Service"
                port 1234
                given "locking"
                uponReceiving "a lock request"
                withAttributes(
                        method: "POST",
                        path: "/api/accounts/123/locks",
                        headers: ['Content-Type': 'application/json'])
                withBody {
                    system regexp("[a-zA-Z0-9]+")
                    money {
                        amount numeric()
                        currency regexp("EUR|PLN|GBP")
                    }
                }
                willRespondWith(
                        status: 201,
                        headers: ['Location': string()],
                )
            }

        when:
            PactVerificationResult result = pactBuilder.runTest {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(new URI("http://localhost:1234/api/accounts/123/locks"))
                        .POST(ofString("""{ "system" : "RESERVATIONS", "money" : {"amount" : 10, "currency" : "EUR"} }"""))
                        .header("Content-Type", "application/json")
                        .build()

                HttpResponse<Void> response = HttpClient.newHttpClient().send(request, discarding())

                assert response.statusCode() == 201
            }

        then:
            result instanceof PactVerificationResult.Ok
    }
}
