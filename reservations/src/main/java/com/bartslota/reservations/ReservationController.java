package com.bartslota.reservations;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

/**
 * @author bslota on 05/01/2022
 */
@RestController
@RequestMapping("/api/reservations")
class ReservationController {

    private final ReservationService reservationService;

    ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> makeReservation(@RequestBody ReservationDto reservation, @RequestHeader(name = AUTHORIZATION, required = false) String auth) {
        return reservationService.makeReservationFor(reservation.vehicleId(), auth);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> cancelReservation(@RequestBody ReservationDto reservation, @RequestHeader(name = AUTHORIZATION, required = false) String auth) {
        return reservationService.cancelReservationFor(reservation.vehicleId(), auth);
    }
}
