package com.bartslota.reservations.payments;

import java.time.Duration;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;

import static java.time.temporal.ChronoUnit.MILLIS;

/**
 * @author bslota on 06/01/2022
 */
@Component
public class PaymentsFacade {

    private final PaymentsClient client;
    private final MeterRegistry meterRegistry;

    PaymentsFacade(PaymentsClient client, MeterRegistry meterRegistry) {
        this.client = client;
        this.meterRegistry = meterRegistry;
    }

    public ResponseEntity<?> send(AccountId accountId, LockRequest request) {
        Timer timer = Timer.builder("account.locks")
                           .tags(List.of(Tag.of("accountId", accountId.value())))
                           .publishPercentiles(0.95, 0.99)
                           .publishPercentileHistogram()
                           .serviceLevelObjectives(Duration.of(300, MILLIS), Duration.of(200, MILLIS))
                           .register(meterRegistry);
        return timer.record(() -> client.send(accountId.value(), request));
    }
}
