package com.bartslota.reservations.payments;

/**
 * @author bslota on 05/01/2022
 */
interface Command {

    String getType();

}
