package com.bartslota.reservations;

import java.math.BigDecimal;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bartslota.reservations.availability.AvailabilityFacade;
import com.bartslota.reservations.availability.LockCommand;
import com.bartslota.reservations.availability.UnlockCommand;
import com.bartslota.reservations.payments.AccountId;
import com.bartslota.reservations.payments.LockRequest;
import com.bartslota.reservations.payments.Money;
import com.bartslota.reservations.payments.PaymentsFacade;

/**
 * @author bslota on 05/01/2022
 */
@Service
class ReservationService {

    private static final Integer DEFAULT_LOCK_DURATION_IN_MINUTES = 10;
    private static final AccountId TECHNICAL_ACCOUNT_ID = new AccountId("abc123");
    private static final Money DEFAULT_MONEY_LOCK_AMOUNT = new Money(new BigDecimal(120), "EUR");
    private static final String SERVICE_NAME = "reservation";

    private final AvailabilityFacade availabilityFacade;
    private final PaymentsFacade paymentsFacade;

    ReservationService(AvailabilityFacade availabilityFacade, PaymentsFacade paymentsFacade) {
        this.availabilityFacade = availabilityFacade;
        this.paymentsFacade = paymentsFacade;
    }

    ResponseEntity<?> makeReservationFor(String vehicleId, String auth) {
        paymentsFacade.send(TECHNICAL_ACCOUNT_ID, new LockRequest(DEFAULT_MONEY_LOCK_AMOUNT, SERVICE_NAME));
        return availabilityFacade.send(new LockCommand(vehicleId, DEFAULT_LOCK_DURATION_IN_MINUTES), auth);
    }

    ResponseEntity<?> cancelReservationFor(String vehicleId, String auth) {
        return availabilityFacade.send(new UnlockCommand(vehicleId), auth);
    }
}
